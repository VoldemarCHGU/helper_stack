from django.shortcuts import render
from django.shortcuts import get_object_or_404
from django.shortcuts import redirect

from .models import *

class ObjectDetailMixin:
    pass

class ObjectCreateMixin:
    model_form = None
    template = None
    redirect_url = 'issues_list_url'

    def get(self, request):
        form = self.model_form()
        return render(request, self.template, context={'form': form})

    def post(self, request):
        bound_form = self.model_form(request.POST)
        if bound_form.is_valid():
            new_object = bound_form.save()
            return redirect(reverse(self.redirect_url))
            # return redirect(new_object)
        return render(request, self.template, context={'form': bound_form})