from django.contrib import admin

# Register your models here.

from .models import *

class IssueAdmin(admin.ModelAdmin):
    list_display = ("id", "title", "date_create", "slug","is_published",)
    list_display_links =["id", 'title', 'slug']
    search_fields = ('title', 'body')
    list_filter = ("date_create", "date_update", "is_published")
    prepopulated_fields = {"slug": ("title",)}
    list_editable = ("is_published",)

admin.site.register(Issue, IssueAdmin)
