from django.contrib.auth import logout
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.views import LoginView
from django.shortcuts import render

# Create your views here.
from django.shortcuts import render
from django.views.generic import View, ListView, CreateView
# Create your views here
from django.urls import reverse, reverse_lazy
from .models import  *
from .forms import *
from .utils import *
from django.shortcuts import redirect
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.paginator import Paginator
from django.db.models import Q
from rest_framework import generics

class IssueList(ListView):
    model = Issue
    template_name = "testing_app/all_issues.html"
    context_object_name = 'all_issues'

    # paginator = Paginator(issues, 5)

    #page_number = request.GET.get('page', 1)
    #page = paginator.get_page(page_number)

    # is_paginated = page.has_other_pages()
    # if page.has_previous():
    #     prev_url = f'?page={page.previous_page_number()}'
    # else:
    #     prev_url = ''
    # if page.has_next():
    #     next_url = f'?page={page.next_page_number()}'
    # else:
    #     next_url = ''



    # context = {
    #     'page_object': page,
    #     'is_paginated': is_paginated,
    #     'next_url': next_url,
    #     'prev_url': prev_url,
    #     'count': len(issues),
    # }
    # return render(request, 'testing_app/all_issues.html', context=context)


class IssueCreate(ObjectCreateMixin, View):
# class PostCreate(LoginRequiredMixin, ObjectCreateMixin, View):
    model_form = IssueForm
    template = 'testing_app/issue_create.html'

class RegisterUser(CreateView):
    form_class = RegisterUserForm
    template_name = "testing_app/register.html"
    success_url = reverse_lazy('login')


class LoginUser(LoginView):
    form_class = LoginUserForm
    template_name = "testing_app/login.html"


    def get_success_url(self):
        return reverse_lazy('home')

def logout_user(request):
    logout(request)
    return redirect('login')