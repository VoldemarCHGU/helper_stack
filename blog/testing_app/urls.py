from django.contrib import admin
from django.urls import path

from .views import *

urlpatterns = [
    path('issues/', IssueList.as_view(), name='issues_list_url'),
    path('issue/create/', IssueCreate.as_view(), name='issue_create_url'),



]