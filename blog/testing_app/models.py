from django.db import models
from django.shortcuts import reverse
from time import time
from django.utils.text import slugify

# Create your models here.

class Issue(models.Model):
    title = models.CharField(max_length=150, db_index=True, verbose_name='Заголовок')

    slug = models.SlugField(max_length=150, unique=True, verbose_name='URL')

    # tags = models.ManyToManyField('Tag', blank=True, related_name='posts')
    body = models.TextField(blank=True, db_index=True, verbose_name='Описание')
    photo = models.ImageField(blank=True, default='', upload_to="photos/model_issue/%Y/%m/%d/", verbose_name='Фото')
    date_create = models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')
    date_update = models.DateTimeField(auto_now=True, verbose_name='Дата изменения')
    is_published = models.BooleanField(default=True, verbose_name='Видимость')

    class Meta:
        verbose_name = 'Задача'
        verbose_name_plural = 'Задачи'
        ordering = ['-date_update']

    def __str__(self):
        return f'{self.title}'

