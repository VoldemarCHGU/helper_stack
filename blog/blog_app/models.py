from django.db import models
from django.shortcuts import reverse
from time import time
from django.utils.text import slugify
from django.core.paginator import Paginator

def gen_slug(s, date=False, space=False):
    s = s.strip()
    s = s.replace(" ", "_")
    new_slug = slugify(s, allow_unicode=True)
    if date:
        return str(''.join([new_slug,'-',str(int(time()))]))
    elif date==False:
        return str(''.join([new_slug]))



class Post(models.Model):
    title = models.CharField(max_length=150, db_index=True, verbose_name='Заголовок')
    slug = models.SlugField(max_length=150, blank=True, unique=True)
    tags = models.ManyToManyField('Tag', blank=True, related_name='posts')
    body = models.TextField(blank=True, db_index=True, verbose_name='Содержание')
    # catalog_for_test = models.ManyToManyField('CatalogForTest', blank=True, related_name='post')
    photo = models.ImageField(upload_to="photos/model_post/%Y/%m/%d/", verbose_name='Фото')
    date_pub = models.DateTimeField(auto_now_add=True, verbose_name='Дата публикации')
    time_update = models.DateTimeField(auto_now=True, verbose_name='Дата изменения')
    is_published = models.BooleanField(default=True, verbose_name='Опубликовано')


    def get_absolute_url(self):
        if self.slug == '':
            self.slug = gen_slug(self.title, True, True)
            self.save()
        return reverse('post_detail_url', kwargs={'slug':self.slug})

    def get_update_url(self):
        return reverse('post_update_url', kwargs={'slug':self.slug})

    def save(self, *args, **kwargs):
        if (not self.id) and (self.slug=='' or self.slug==None):
            self.slug = gen_slug(self.title, date=True)
        super().save(*args, **kwargs)

    def get_delete_url(self):
        return reverse('post_delete_url', kwargs={'slug': self.slug})


    def __str__(self):
        return f'{self.title}'

    class Meta:
        verbose_name = 'Пост'
        verbose_name_plural = 'Посты'
        ordering = ['-date_pub']


class Tag(models.Model):
    title = models.CharField(max_length=50)
    slug = models.SlugField(max_length=50, unique=True)

    def get_absolute_url(self):
        return reverse('tag_detail_url', kwargs={'slug': self.slug})


    def get_update_url(self):
        return reverse('tag_update_url', kwargs={'slug':self.slug})

    def get_delete_url(self):
        return reverse('tag_delete_url', kwargs={'slug': self.slug})

    def __str__(self):
        return f'{self.title}'

    class Meta:
        ordering = ['title']



class CatalogPost(models.Model):
    title = models.CharField(max_length=150, verbose_name='Заголовок')
    slug = models.SlugField(max_length=150, unique=True)
    category = models.ManyToManyField('Post', blank=True, related_name='category', verbose_name='Категория')
    # date_start = models.DateTimeField(blank=True, verbose_name='Дата начала')
    # date_end = models.DateTimeField(blank=True, verbose_name='Дата завершения')
    # is_publish = models.BooleanField(default=False, verbose_name='Опубликовано')

    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'категории'
        ordering = ['title']

    def __str__(self):
        return f'{self.title}'

    def get_absolute_url(self):
        return reverse('post_category', kwargs={'pk': self.pk})
