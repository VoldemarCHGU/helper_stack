from django.shortcuts import render
from django.views.generic import View
# Create your views here
from django.urls import reverse
from .models import Post, Tag
from .serializers import PostSerializer
from .utils import *
from .forms import TagForm, PostForm
from django.shortcuts import redirect
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.paginator import Paginator
from django.db.models import Q
from rest_framework import generics

def posts_list(request):
    search_query = request.GET.get("search", "").lower()
    if search_query:

        posts = Post.objects.filter(Q(title__icontains=search_query) | Q(body__icontains=search_query) )
    else:
        posts = Post.objects.all().prefetch_related('tags')

    paginator = Paginator(posts,19 )

    page_number = request.GET.get('page', 1)
    page = paginator.get_page(page_number)

    is_paginated = page.has_other_pages()
    if page.has_previous():
        prev_url = f'?page={page.previous_page_number()}'
    else:
        prev_url = ''
    if page.has_next():
        next_url = f'?page={page.next_page_number()}'
    else:
        next_url = ''

    category = CatalogPost.objects.all()

    context = {
        'page_object': page,
        'is_paginated': is_paginated,
        'next_url': next_url,
        'prev_url': prev_url,
        'count': len(posts),
        'catalog_for_post':category,
    }
    return render(request, 'blog_app/index.html', context=context)

class PostDetail(ObjectDetailMixin, View):
    model = Post
    template = 'blog_app/post_detail.html'

class PostCreate(ObjectCreateMixin, View):
# class PostCreate(LoginRequiredMixin, ObjectCreateMixin, View):
    model_form = PostForm
    template = 'blog_app/post_create.html'
    # raise_exception = True

    # def get(self, request):
    #     form = PostForm()
    #     return render(request, 'blog_app/post_create.html', context={'form': form})
    #
    # def post(self, request):
    #     bound_form = PostForm(request.POST)
    #     if bound_form.is_valid():
    #         new_post = bound_form.save()
    #         return redirect(new_post)
    #     return render(request, 'blog_app/post_create.html', context={'form': bound_form})

class PostUpdate(ObjectUpdateMixin, View):
# class PostUpdate(LoginRequiredMixin, ObjectUpdateMixin, View):
    model = Post
    model_form = PostForm
    template = 'blog_app/post_update_form.html'
    # raise_exception = True

class PostDelete(LoginRequiredMixin, ObjectDeleteMixin, View):
    model = Post
    template = "blog_app/post_delete_form.html"
    redirect_url = 'post_list_url'
    raise_exception = True

class TagDelete(LoginRequiredMixin, ObjectDeleteMixin, View):
    model = Post
    template = "blog_app/post_delete_form.html"
    redirect_url = 'post_list_url'
    raise_exception = True

class TagDetail(ObjectDetailMixin, View):
    model = Tag
    model_form = TagForm
    template = 'blog_app/tag_detail.html'

def tags_list(request):
    tags = Tag.objects.all()
    return render(request, 'blog_app/tags_list.html', context={'tags': tags})

class TagCreate(LoginRequiredMixin, ObjectCreateMixin, View):
    model_form = TagForm
    template = 'blog_app/tag_create.html'
    raise_exception = True

class TagUpdate(LoginRequiredMixin, ObjectUpdateMixin, View):
    model = Tag
    model_form = TagForm
    template = 'blog_app/tag_update_form.html'
    raise_exception = True

class TagDelete(LoginRequiredMixin, ObjectDeleteMixin, View):
    model = Tag
    template = "blog_app/tag_delete_form.html"
    redirect_url = 'tags_list_url'
    raise_exception = True

class PostAPIView(generics.ListAPIView):
    queryset = Post.objects.all()
    serializer_class = PostSerializer

def PostCategory(request, pk):

    posts = Post.objects.filter(category=pk)

    paginator = Paginator(posts,5)

    page_number = request.GET.get('page', 1)
    page = paginator.get_page(page_number)

    is_paginated = page.has_other_pages()
    if page.has_previous():
        prev_url = f'?page={page.previous_page_number()}'
    else:
        prev_url = ''
    if page.has_next():
        next_url = f'?page={page.next_page_number()}'
    else:
        next_url = ''

    category = CatalogPost.objects.all()

    context = {
        'page_object': page,
        'is_paginated': is_paginated,
        'next_url': next_url,
        'prev_url': prev_url,
        'count': len(posts),
        'catalog_for_post': category,
    }
    return render(request, 'blog_app/index.html', context=context)