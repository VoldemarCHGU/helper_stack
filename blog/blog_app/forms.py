from django import forms
from .models import Tag, Post
from django.core.exceptions import ValidationError

class TagForm(forms.ModelForm):

    class Meta:
        model = Tag
        fields = ['title', 'slug']

        widgets = {
            'title':forms.TextInput(attrs={'class':'form-control'}),
            'slug': forms.TextInput(attrs={'class': 'form-control'})
        }

    # def save(self):
    #     new_tag = Tag.objects.create(title=self.cleaned_data['title'], slug=self.cleaned_data['slug'])
    #     return new_tag

    def clean_slug(self):
        new_slug = self.cleaned_data['slug'].lower()
        if new_slug == 'create':
            raise ValidationError('Slug may not be "Create"')
        if (self.cleaned_data['slug'].lower()==self.instance.slug):
            return new_slug
        if Tag.objects.filter(slug__iexact=new_slug).count():
            raise ValidationError(f"We have '{new_slug}' slug")
        return new_slug


class PostForm(forms.ModelForm):

    class Meta:
        model = Post
        fields = ['title', 'slug', 'body', 'tags',]

        widgets = {
            'title':forms.TextInput(attrs={'class':'form-control'}),
            'slug': forms.TextInput(attrs={'class': 'form-control'}),
            'body': forms.Textarea(attrs={'class': 'form-control'}),
            'tags': forms.SelectMultiple(attrs={'class': 'form-control'}),
            
        }

    #мустым поле может быть, автогенерация слага присутствует
    def clean_slug(self):
        new_slug = self.cleaned_data['slug'].lower()
        if new_slug == 'create':
            raise ValidationError('Slug may not be "Create"')
        return new_slug