from django.contrib import admin

# Register your models here.
from .models import *



class PostAdmin(admin.ModelAdmin):
    list_display = ("id", "title", "date_pub","time_update", "is_published", "photo")
    list_display_links =["id", 'title']
    search_fields = ('title', 'body')
    list_filter = ("date_pub","time_update", "is_published")
    list_editable = ("is_published",)
    prepopulated_fields = {"slug": ("title",)}


class CatalogPostAdmin(admin.ModelAdmin):
    list_display = ("id", "title",)
    list_display_links =["id", 'title']
    search_fields = ('title',)
    prepopulated_fields = {"slug": ("title",)}

admin.site.register(Post, PostAdmin)
admin.site.register(CatalogPost, CatalogPostAdmin)

