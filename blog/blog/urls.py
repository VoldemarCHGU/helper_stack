"""blog URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the includes() function: from django.urls import includes, path
    2. Add a URL to urlpatterns:  path('blog/', includes('blog.urls'))
"""
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include


from blog_app.views import PostAPIView
from testing_app.views import *
from . import settings
from .views import *

urlpatterns = [
    path('', home, name='home'),
    path('admin/', admin.site.urls),
    path('', include('blog_app.urls')),
    path('', include('testing_app.urls')),

    path('registration/', RegisterUser.as_view(), name='register'),
    path('login/', LoginUser.as_view(), name='login'),
    path('logout/', logout_user, name='logout'),

    path('api/v1/postlist/', PostAPIView.as_view(), name='api_post_list_url_v1'),
    path('api/v2/postlist/', PostAPIView.as_view(), name='api_post_list_url_v2'),

]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [
        path('___debug__/', include((debug_toolbar.urls)))
    ] + urlpatterns

    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
